//
//  AppDelegate.h
//  tilosgame
//
//  Created by IT-Högskolan on 2015-03-26.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

