//
//  FoodiesTableController.m
//  Foodie
//
//  Created by IT-Högskolan on 2015-02-17.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import "FoodiesTableController.h"
#import "FoodiesList.h"
#import "DetailViewController.h"
#import "FoodiesFavorites.h"
#import "GetFoodiesController.h"

@interface FoodiesTableController ()



@property (nonatomic) NSMutableArray* foodiesAr;

@property (nonatomic) NSMutableArray* foodiesArr;

@property (strong, nonatomic) NSDictionary *foodiesDict;

@end

@implementation FoodiesTableController

@synthesize foodiesDict = _foodiesDict;
@synthesize foodiesArr = _foodiesArr;

- (void)viewWillAppear:(BOOL)animated {
    [self.tableView reloadData];
    
   // NSLog(@"Will Appear called");
    
}

- (NSMutableArray *) foodiesArr {
    
  //  NSLog(@"fodiesArr Called: %@", _foodiesArr[0]);
    
    return _foodiesArr;
}

- (void) setFoodiesDict: (NSDictionary*)newValue {
    
    
    
    //   NSDictionary *dict = @{@"key1":@"Eezy",@"key2": @"Tutorials"};
    
    _foodiesDict =      @{@"name":   @"newValue",
                          @"name":   @"newValue",
                          @"name":   @"newValue newValue",
                          @"name": @[@"Objective C", @"Unity"]};
    
    _foodiesDict = newValue;
  //  NSLog(@"Table Foodie: %@", self.foodiesDict[@"name"]);
    
}

- (NSDictionary *) foodiesDict {
    
    return _foodiesDict;
}



/*
- (NSArray*) FoodiesArrFROMDICTIONARY: (NSDictionary*)newValue {
    
    _foodiesArr = [@[@"Horse",@"Shark",@"Cat",@"foo"] mutableCopy];
    
    if(!self.foodiesArr) {
        NSArray *keys = [self.foodiesDict allKeys];
        int i;
        
        for(i = 0; [self.foodiesDict count]; i++) {
            
            _foodiesArr[i] = keys[i];
        }
    
    }
    return _foodiesArr;
}
*/



- (void)viewDidLoad {
    [super viewDidLoad];
    
  //  [self setFoodiesArr: (NSArray*) foobar[0] = {@"foobar"}];
  //  [self setFoodiesArr];
    
    
  //  NSMutableArray *foobar = [self.foodiesDict allKeys];
    
//    [self setFoodiesArr: keys];
    
 //   [self setFoodiesArr: [self foodiesDict]];
    
 //   _foodiesArr = [@[@"Horse",@"Shark",@"Cat",@"foo"] mutableCopy];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.foodiesArr.count;

    
}
/*
 - (void)tableView:(UITableView *)tableView willDispayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 cell.contentView.backgroundColor = [UIColor blueColor];
 }
 */

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCell" forIndexPath:indexPath];
    
    int index = indexPath.row;
    
    // Here we fill the cell with tasks
    
    cell.textLabel.text = @"aha";
    cell.textLabel.text = self.foodiesArr[index];
    
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor:[UIColor redColor]];
    [cell setSelectedBackgroundView:bgColorView];
    
    if (indexPath.row%2 == 0) {
        UIColor *altCellColor = [UIColor colorWithWhite:0.7 alpha:0.1];
        cell.backgroundColor = altCellColor;
 //       cell.detailTextLabel = @"foobar";
    }
    
    return cell;
}
/*
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
 {
 static NSString *CellIdentifier = @"Cell";
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
 
 // Configure the cell...
 cell.textLabel.text = [self.data objectAtIndex:indexPath.row];
 
 // change background color of selected cell
 UIView *bgColorView = [[UIView alloc] init];
 [bgColorView setBackgroundColor:[UIColor redColor]];
 [cell setSelectedBackgroundView:bgColorView];
 
 return cell;
 }
 */

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    DetailViewController *target = segue.destinationViewController;
      NSLog(@"prepareForSegue Called: %@", self.foodiesArr[0]);
    
    
    
    if([segue.identifier isEqualToString:@"details"]) {
        
        DetailViewController *nextController = [segue destinationViewController];
        UITableViewCell *cell = sender;
        
      //  nextController.lblSearchFoodie.text = cell.textLabel.text;
        nextController.searchStr = cell.textLabel.text;
        
     //   [target.lblSearchFoodie.text = self cell.textLabel.text];
        
        nextController.lblSearchFoodie.textColor = [UIColor redColor];
        
    //    [target setFoodiesArr:self.foodiesArr];
        
        NSLog(@"Cell sent: %@", cell.textLabel.text);
     //   NSLog(@"Cell sent2: %@", nextController.lblSearchFoodie.text); // not working
        
        
        cell.contentView.backgroundColor=[UIColor blueColor];
    }
    
    else if([segue.identifier isEqualToString:@"favorites"]) {
      // FoodiesFavorites *nextController = [segue destinationViewController];
       // nextController.foodiesDict = self.foodiesDict;
    }
   // else NSLog(@"You forgot the segue %@", segue);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIAlertView *messageAlert = [[UIAlertView alloc]
                                 initWithTitle:@"Row Selected" message:@"You've selected a row" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    // Display Alert Message
//    [messageAlert show];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    NSLog(@"Cell Pressed: %@", cell.textLabel.text);
    
    
    
  //  [self getDetails];
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   // NSLog(@"Table View called");
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
 //       [self.foodiesArr removeObjectAtIndex:indexPath.row];
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}
@end
