//
//  DetailViewController.m
//  Foodie
//
//  Created by IT-Högskolan on 2015-02-17.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()



@property (weak, nonatomic) IBOutlet UILabel *lblSearchStr;
@property (weak, nonatomic) IBOutlet UILabel *key1;
@property (weak, nonatomic) IBOutlet UILabel *key2;
@property (weak, nonatomic) IBOutlet UILabel *key3;

@property (weak, nonatomic) IBOutlet UILabel *value1;
@property (weak, nonatomic) IBOutlet UILabel *value2;
@property (weak, nonatomic) IBOutlet UILabel *value3;

@property (weak, nonatomic) IBOutlet UILabel *nyttig;

@property (weak, nonatomic) IBOutlet UILabel *value4;

@property (nonatomic) NSArray* root;
@property (nonatomic) NSMutableDictionary* rootNut;
@property NSString* searchNum;
@property (nonatomic) NSMutableArray* mArrResult;


@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.lblSearchStr.text = self.searchStr;
    
    [self getFoodies: self.searchStr];
    
    
    
}

- (void) getFoodies: (NSString *)s {

    NSLog(@"Received lblSearchstr: %@",self.lblSearchStr.text);
    
    NSLog(@"s = %@",s);

    NSString *searchString = [NSString stringWithFormat:@"http://www.matapi.se/foodstuff/?query=%@", s];
    
    //   http://www.matapi.se/foodstuff?query=44&format=json&pretty=0
    
    NSString *safeSearchString = [searchString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"safeSearchSvtring: %@",safeSearchString);
    NSURL        *url     = [NSURL URLWithString: safeSearchString];
    NSURLRequest *request = [NSURLRequest requestWithURL: url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSLog(@"Got data! Data: %@, error: %@",data,error);
        
        // Kolla om man har fått ett fel först
        
        if(error) NSLog(@"error");
        
        NSError *parseError;
        
        self.root = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
        
        if(!parseError) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(self.root.count > 0) {
                    
                    self.foodiesArr = [[NSMutableArray alloc] init];
                    
                    for(int i = 0; i < self.root.count; i++) {
                        
                        [self.foodiesArr addObject:self.root[i][@"number"]];
                        self.searchNum = self.foodiesArr[i];
                        NSLog(@"foodiesArrDetail: %@", self.foodiesArr[i]);
                        NSLog(@"search num: %@", self.searchNum);
                        [self getFoodiesNut: self.searchNum];
                        
                        //   [self.foodiesArr addObject: self.root[i][@"name"]]; // the other way
                        
                    }
           //         self.textSearchResult.text = self.root[0][@"name"]; // ändra
                    
                } else {
             //       self.textSearchResult.text = @"No topics found.";
                }
                
           //     [self performSegueWithIdentifier:@"NextController" sender:self];
            });
        } else {
            NSLog(@"Couldn't parse json: %@",parseError);
        }
        
    }];
    
    [task resume];
    
    
 
}

- (void) getFoodiesNut: (NSString *)s {
    
    NSLog(@"s in foodiesnut = %@",s);
    
    NSString *searchString = [NSString stringWithFormat:@"http://www.matapi.se/foodstuff/%@", s];
        
    NSString *safeSearchString = [searchString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"safeSearchString: %@",safeSearchString);
    
    NSURL *url = [NSURL URLWithString: safeSearchString];
    NSURLRequest *request = [NSURLRequest requestWithURL: url];
    NSURLSession *session = [NSURLSession sharedSession];    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSLog(@"Got data! Data: %@, error: %@",data,error);
        
        if(error) NSLog(@"error");
        
        NSError *parseError;
        
        self.rootNut = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
        
        if(!parseError) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSMutableDictionary* nutrientDict = [[NSMutableDictionary alloc] init];
                
                nutrientDict = self.rootNut[@"nutrientValues"];
                NSLog(@"nutrientValues: %@: %@",nutrientDict.allKeys, nutrientDict.allValues);
                
                self.key1.text = @"carbohydrates";
                self.key2.text = @"protein";
                self.key3.text = @"iron";
                
                float nValue;
                NSNumber *num;
                
                
                num = nutrientDict[@"carbohydrates"];
                //NSLog(@"str is a %@", [str class]);
                self.value1.text = [NSString stringWithFormat:@"%@",num];
                nValue = [num floatValue];
                NSLog(@"nValue1: %f",nValue);
                
                num = nutrientDict[@"protein"];
                self.value2.text = [NSString stringWithFormat:@"%@",num];
                nValue = nValue + [num floatValue];
                NSLog(@"nValue2: %f",nValue);
                
                num = nutrientDict[@"iron"];
                self.value3.text = [NSString stringWithFormat:@"%@",num];
                nValue = nValue + [num floatValue];
                NSLog(@"nValue3: %f",nValue);
                
                
                self.value4.text = [NSString stringWithFormat:@"%f",nValue];
                
                NSLog(@"nValue: %f",nValue);
                
            });
        } else {
            NSLog(@"Couldn't parse json: %@",parseError);
        }
        
    }];

    [task resume];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
