//
//  FoodiesFavorites.m
//  Foodie
//
//  Created by IT-Högskolan on 2015-02-21.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import "FoodiesFavorites.h"
#import "FoodiesList.h";

@interface FoodiesFavorites ()

@property (strong, nonatomic) NSDictionary *foodiesDict;

@end

@implementation FoodiesFavorites

@synthesize foodiesDict = _foodiesDict;

- (void)viewWillAppear:(BOOL)animated {
    [self.tableView reloadData];
    
    NSLog(@"Will Appear called");
    
    
}

- (void) setFoodiesDict: (NSDictionary*)newValue {
    
    //   NSDictionary *dict = @{@"key1":@"Eezy",@"key2": @"Tutorials"};
    
    _foodiesDict =      @{@"name":   @"newValue",
                          @"name":   @"newValue",
                          @"name":   @"newValue newValue",
                          @"name": @[@"Objective C", @"Unity"]};
    
    _foodiesDict = newValue;
    
}

- (NSDictionary *) foodiesDict {
    
    return _foodiesDict;
}

- (NSDictionary*) setFoodies {
    
    self.foodiesDict = [[FoodiesList  alloc] init];
    
    
    if(!self.foodiesDict) {
        //    self.foodiesDict = [foo tasks];
    }
    return _foodiesDict;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
