//
//  DetailViewController.h
//  Foodie
//
//  Created by IT-Högskolan on 2015-02-17.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (nonatomic) NSMutableArray* foodiesArr;
@property (weak, nonatomic) IBOutlet UILabel *lblSearchFoodie;
@property (nonatomic) NSString * searchStr;
@end
