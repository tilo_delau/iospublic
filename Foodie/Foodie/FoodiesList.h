//
//  FoodiesList.h
//  Foodie
//
//  Created by IT-Högskolan on 2015-02-17.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FoodiesList : NSObject

// -(NSMutableArray*) FoodiesMutableArr;

- (NSMutableArray*) FoodiesArr;
- (void) setFoodiesDict: (NSDictionary*)newValue;
- (void) setFoodiesDict;
- (NSDictionary *) foodiesDict;





@end
