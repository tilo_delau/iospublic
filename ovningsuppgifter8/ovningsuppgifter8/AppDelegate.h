//
//  AppDelegate.h
//  ovningsuppgifter8
//
//  Created by IT-Högskolan on 2015-02-12.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

