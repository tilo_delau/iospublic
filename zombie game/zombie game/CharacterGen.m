//
//  CharacterGen.m
//  zombie game
//
//  Created by IT-Högskolan on 2015-01-29.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import "CharacterGen.h"

@implementation CharacterGen

- (NSString*) charBio {
    
    NSString *day   = [self getDay];
    NSString *fname = [self getFname];
    NSString *work = [self getWork];
    NSString *lname = [self getLname];
    NSString *disturbance = [self getDisturbance];
    NSString *coffee = [self getCoffee];
    NSString *bag = [self getBag];
    NSString *equip = [self getEquip];
    NSString *romance = [self getRomance];
    NSString *barista = [self getBarista];
    NSString *smiled = [self getSmile];
    NSString *well = [self getWell];

    
    NSString *s =
    [NSString stringWithFormat:@"The %@ seemed to start out well enough. %@ was on time and heading to work\r "
     "at the %@. As he pulled into the Drive-Thru, the radio was interuppted by news reports: \r"
     "'Police and emergency services responded to %@ taking place down town...'\r"
     "'Why can't the protestors do it peacefully', thought %@, as he checked his %@ to be sure that he packed his %@.\r"
     "'Good evening Mr. %@,' said the barista girl, '%@ as usual?'\r"
     "'Perfect!', %@ replied, 'Thank you, %@. I'll see you tommorow!'\r"
     "'%@', replied %@.\r"
     "%@ grabbed his %@ and %@ to himself as he pulled out to the main road.\r"
     "Things are just going to go %@ today, %@ thought."
     
     ,day, fname, work, disturbance, fname, bag, equip, lname, coffee, fname, barista, romance, barista, fname, coffee, smiled, well, fname ];

    return s;
    
}


- (NSString*) getDay {
    
    NSArray *myArray = @[@"day", @"morning", @"evening"];
    
    return [NSString stringWithFormat: @"%@",myArray[arc4random_uniform([myArray count])]];
}


- (NSString*) getFname {
    
    NSArray *myArray = @[@"Adam", @"Bill", @"David", @"John", @"Hannibal", @"Sam"];
    
    return [NSString stringWithFormat: @"%@",myArray[arc4random_uniform([myArray count])]];
}
- (NSString*) getWork {
    
    NSArray *myArray = @[@"warehouse", @"circus", @"hospital", @"fire station", @"office"];
    
    return [NSString stringWithFormat: @"%@",myArray[arc4random_uniform([myArray count])]];
}

- (NSString*) getLname {
    
    NSArray *myArray = @[@"Homes", @"Copley", @"Rogan", @"Hoffmeister", @"Dearing", @"Evers"];
    
    return [NSString stringWithFormat: @"%@",myArray[arc4random_uniform([myArray count])]];
}

- (NSString*) getDisturbance {
    
    NSArray *myArray = @[@"riots", @"homicides", @"protests", @"arsonists", @"murders"];
    
    return [NSString stringWithFormat: @"%@",myArray[arc4random_uniform([myArray count])]];
}

- (NSString*) getCoffee {
    
    NSArray *myArray = @[@"coffee", @"latté", @"cafe moccha", @"french vanilla latté"];
    
    return [NSString stringWithFormat: @"%@",myArray[arc4random_uniform([myArray count])]];
}
- (NSString*) getBag {
    
    NSArray *myArray = @[@"sportbag", @"briefcase", @"back pack", @"duffel bag"];
    
    return [NSString stringWithFormat: @"%@",myArray[arc4random_uniform([myArray count])]];
}
- (NSString*) getEquip {
    
    NSArray *myArray = @[@"gun", @"hammer", @"calculator", @"lunch"];
    
    return [NSString stringWithFormat: @"%@",myArray[arc4random_uniform([myArray count])]];
}
- (NSString*) getRomance {
    
    NSArray *myArray = @[@"Would love that!", @"See you then!", @"Looking forward to it!", @"I hope not."];
    
    return [NSString stringWithFormat: @"%@",myArray[arc4random_uniform([myArray count])]];
}
- (NSString*) getBarista {
    
    NSArray *myArray = @[@"Emma", @"Claire", @"Megan", @"Julie"];
    
    return [NSString stringWithFormat: @"%@",myArray[arc4random_uniform([myArray count])]];
}
- (NSString*) getWell {
    
    NSArray *myArray = @[@"swell", @"horrible", @"great"];
    
    return [NSString stringWithFormat: @"%@",myArray[arc4random_uniform([myArray count])]];
}
- (NSString*) getSmile {
    
    NSArray *myArray = @[@"smiled", @"frowned", @"grinned"];
    
    return [NSString stringWithFormat: @"%@",myArray[arc4random_uniform([myArray count])]];
}

@end
