//
//  ViewController.m
//  zombie game
//
//  Created by IT-Högskolan on 2015-01-22.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import "ViewController.h"
#include <stdlib.h>
#import "CharacterGen.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIView *colorPad;
@property (weak, nonatomic) IBOutlet UISlider *redSlider;
@property (weak, nonatomic) IBOutlet UISlider *greenSlider;
@property (weak, nonatomic) IBOutlet UISlider *blueSlider;
@property (weak, nonatomic) IBOutlet UILabel *redLabel;
@property (weak, nonatomic) IBOutlet UILabel *blueLabel;
@property (weak, nonatomic) IBOutlet UILabel *greenLabel;
@property (weak, nonatomic) IBOutlet UIButton *sliderBackBtn;


@property (weak, nonatomic) IBOutlet UIButton *killBtn;
@property (weak, nonatomic) IBOutlet UIButton *runBtn;

@property (weak, nonatomic) IBOutlet UIImageView *zombie;
@property (weak, nonatomic) IBOutlet UILabel *tBrains;
@property (weak, nonatomic) IBOutlet UITextView *tvStory;
@property (weak, nonatomic) IBOutlet UIButton *leaveBtn;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property (weak, nonatomic) IBOutlet UIImageView *zombieBlue;
@property (weak, nonatomic) IBOutlet UIButton *killZBtn;
@property (weak, nonatomic) IBOutlet UITextView *tvNom;
@property (weak, nonatomic) IBOutlet UITextView *charBio;
@property (weak, nonatomic) IBOutlet UIButton *changeCharBtn;

@end

@implementation ViewController

NSString *bio;

- (IBAction)changeChar:(id)sender {
    
 //   [self setCharBio];

    
    
    CharacterGen* cGen = [[CharacterGen alloc] init];
    
    
    
    self.charBio.text = [cGen charBio];
    
}



- (UIColor*) currentColor {
    return [UIColor colorWithRed:self.redSlider.value
                            green:self.greenSlider.value
                            blue:self.blueSlider.value
                            alpha:1.0f];

}

- (void) zombieDie {
    
    [UIView animateWithDuration:2.0f animations:^{
        self.zombieBlue.alpha = 0.0f;
    }];
    
}
- (void) fight {
    
    int r = arc4random_uniform(100);
    
    if (r < 20) {
        [self zombieDie];
        self.killZBtn.hidden = YES;
        self.tvNom.hidden = YES;
    }
    else
        [self.killZBtn setTitle:@"Missed! Kill it again!" forState:UIControlStateNormal];

}

- (IBAction)killZBtn:(id)sender {
    
    [self fight];
    
}


- (IBAction)Search:(id)sender {
    
    self.zombieBlue.hidden = NO;
    self.zombieBlue.alpha = 0.0f;
    
    self.searchBtn.hidden = YES;
    
    [self animateZombie];
}

- (void) animateZombie {

    // Move the zombie to the left
    [UIView animateWithDuration:3.0f animations:^{
        
        self.zombieBlue.alpha = 1.0f;
        self.zombieBlue.frame = CGRectOffset(_zombieBlue.frame, -275, 0);
        
        // Chain next animation, move right and down
        }completion:^(BOOL finished){
            
            
            self.killZBtn.hidden = NO;
            
            
            
            [UIView animateWithDuration:5.0f animations:^{
                self.zombieBlue.frame = CGRectOffset(_zombieBlue.frame, 50, 125);
            
            }];
            self.tvNom.hidden = NO; // No difference to put anything here, same time as above
    }];
    
    // Scale up the zombie during the movement
    [UIView animateWithDuration:8.0f animations:^{
        self.zombieBlue.transform = CGAffineTransformMakeScale(6.5, 6.5);
    }];
    


/* This will shrink the view then chain and unshrink animation. Maybe handy with bouncy balls.
 
    [UIView animateWithDuration:4.0f animations:^{
        self.zombieBlue.transform = CGAffineTransformMakeScale(0.01, 0.01);
    }completion:^(BOOL finished){
        // Finished scaling down imageview, now resize it
        [UIView animateWithDuration:0.8 animations:^{
            self.zombieBlue.transform = CGAffineTransformIdentity;
        }completion:^(BOOL finished){
            NSLog(@"Finished...");
        }];
    }];
*/
 
}



- (IBAction)killZ:(id)sender {
    
    self.tvStory.text = [NSString stringWithFormat:@"You killed the zombie!"];
    
    self.zombie.alpha = 0.5f;
    [UIView animateWithDuration:2.0f animations:^{
        self.zombie.frame = CGRectOffset(_zombie.frame, 0, 250);
    }];
    
    
    self.tBrains.hidden     = YES;
    self.killBtn.hidden     = YES;
    self.runBtn.hidden      = YES;
    self.leaveBtn.hidden    = NO;
    
    // This line makes my zombie animation fall from above. I don't know why.
//    [self.killBtn setTitle:@"Leave" forState:UIControlStateNormal];
    
}

- (IBAction)redSliderChanged:(id)sender {
    self.colorPad.backgroundColor = [self currentColor];
    self.redLabel.text = [NSString stringWithFormat:@"%.2f",self.redSlider.value];
    self.greenLabel.text = [NSString stringWithFormat:@"%.2f",self.greenSlider.value];
    self.blueLabel.text = [NSString stringWithFormat:@"%.2f",self.blueSlider.value];
    
}
- (IBAction)greenSliderChanged:(id)sender {
    self.colorPad.backgroundColor = [self currentColor];
    self.redLabel.text = [NSString stringWithFormat:@"%.2f",self.redSlider.value];
    self.greenLabel.text = [NSString stringWithFormat:@"%.2f",self.greenSlider.value];
    self.blueLabel.text = [NSString stringWithFormat:@"%.2f",self.blueSlider.value];
}
- (IBAction)blueSliderChanged:(id)sender {
    self.colorPad.backgroundColor = [self currentColor];
    self.redLabel.text = [NSString stringWithFormat:@"%.2f",self.redSlider.value];
    self.greenLabel.text = [NSString stringWithFormat:@"%.2f",self.greenSlider.value];
    self.blueLabel.text = [NSString stringWithFormat:@"%.2f",self.blueSlider.value];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self changeChar: self];
    

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
}

@end
