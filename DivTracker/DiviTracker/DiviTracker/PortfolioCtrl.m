//
//  PortfolioCtrl.m
//  DiviTracker
//
//  Created by IT-Högskolan on 2015-07-17.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import "PortfolioCtrl.h"
#import "PortfolioCell.h"


@interface PortfolioCtrl ()

@property (strong, nonatomic) NSMutableArray *arrPortfolio;
@property (strong, nonatomic) NSString *searchString;
@property (strong, nonatomic) NSString *stockName;
@property (strong, nonatomic) NSDictionary *dictJson3;
@property (strong, nonatomic) NSArray *arrJson;


@end

@implementation PortfolioCtrl


- (IBAction)toggleEditing:(id)sender {
    NSLog(@"toggling mode");
    [self setEditing: !self.editing animated: YES];
}

-(void) setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing: editing animated: animated];
    
    const UIBarButtonSystemItem systemItem =
    editing ?
    UIBarButtonSystemItemDone :
    UIBarButtonSystemItemEdit;
    
    UIBarButtonItem *const newButton =
    [[UIBarButtonItem alloc]
     initWithBarButtonSystemItem: systemItem
     target: self
     action: @selector(toggleEditing:)];
    
    [self.navigationItem setRightBarButtonItems: @[newButton] animated: YES];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath { //implement the delegate method
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Update data source array here, something like [array removeObjectAtIndex:indexPath.row];
        
        [self.arrPortfolio removeObjectAtIndex:indexPath.row];
        
        [self saveUserDefault];
        
        NSLog(@"editing: self arrPortfolio: %@", self.arrPortfolio);
        
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
}

- (void) saveUserDefault {
    NSUserDefaults *userDefaults  = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject: self.arrPortfolio forKey:@"portfolio"];
    // Save to file
    [userDefaults synchronize];
    NSLog(@"PortfolioCtrl self.arrPortfolio: %@", self.arrPortfolio);
    
}
- (void) loadUserDefaults {
    
    NSUserDefaults *userDefaults  = [NSUserDefaults standardUserDefaults];
    NSLog(@"PortfolioCtrl Check Portfolio userDefaults: %@", [userDefaults objectForKey:@"portfolio"]);
    self.arrPortfolio = [[userDefaults arrayForKey:@"portfolio"] mutableCopy];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"PortfolioCtrl viewDidLoad");
    

}

- (void) viewWillAppear:(BOOL)animated {
    // This is the edit button!
    [self setEditing: NO animated: NO];
    
    NSLog(@"PortfolioCtrl viewWillAppear called");
    [self loadUserDefaults];
    
    if(self.arrPortfolio.count) {
        NSLog(@"portfolio is not empty %lu, %@", (unsigned long)self.arrPortfolio.count, self.arrPortfolio);
        [self createStockName];
        [self createStockObj];
    }
    else { NSLog(@"Portfolio is empty"); };

  //  [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)createStockName {
    
    self.stockName = @"";
    
    for(int i=0; i < self.arrPortfolio.count; i++) {
        
        if(i > 0) self.stockName =  [self.stockName stringByAppendingString:@"+"];
        self.stockName = [self.stockName stringByAppendingString:[self.arrPortfolio objectAtIndex:i]];
        
/*        self.stockName = [self.stockName stringByAppendingString:[[self.arrPortfolio objectAtIndex:i] objectForKey:@"ticker"] ];
 */
        NSLog(@"stockName: %@", self.stockName);
        
 //        NSLog(@"shares: %@", [[self.portfolio objectAtIndex:self.portfolio.count-1] objectForKey:@"shares"]);
    }
}

- (void)createStockObj {
    

    [self.activityIndicator startAnimating];
    self.activityIndicator.hidden = NO;
    
    NSLog(@"createStockObj called");
    NSLog(@"stockName: %@", self.stockName);
    /*
     NSString *searchString = @"http://query.yahooapis.com/v1/public/yql?q=select*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22STAR.ST%22)%0A%09%09&env=http%3A%2F%2Fdatatables.org%2Falltables.env&format=json";
     */
    //    NSString *searchString = @"http://query.yahooapis.com/v1/public/yql?q=select*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22XOM%22)%0A%09%09&env=http%3A%2F%2Fdatatables.org%2Falltables.env&format=json";
    
    
    // % in url interferes with stringWithFormat. Easiest way is to break up into 3 strings
    NSString *searchStr1 = @"http://query.yahooapis.com/v1/public/yql?q=select*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22";
    NSString *searchStr2 = @"%22)%0A%09%09&env=http%3A%2F%2Fdatatables.org%2Falltables.env&format=json";
    
    NSString *searchString = [NSString stringWithFormat: @"%@%@%@", searchStr1,self.stockName,searchStr2];
    
    
    NSURL                *url     = [NSURL URLWithString:searchString];
    NSURLRequest         *request = [NSURLRequest requestWithURL:url];
    NSURLSession         *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task    = [session dataTaskWithRequest:request completionHandler:^
                                     (NSData *data, NSURLResponse *response, NSError *error)
                                     {
                                         
                                         NSLog(@"ERROR: %@", error);
                                         
                                         NSError *parsingError = error;
                                         
                                         NSDictionary *root =
                                         [NSJSONSerialization JSONObjectWithData:
                                          data options:kNilOptions error: &parsingError];
                                         
                                         if (!parsingError) {
                                             NSLog(@"Start Parsing Json");
                                             
                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                 
                                                 //Put the query json obj in a dict.
                                                 NSDictionary *dict = [root objectForKey:@"query"];
                                                 
                                                 // "created" is in the first dict
                                                 NSLog(@"dict: created: %@", [dict objectForKey:@"created"]);
                                                 
                                                 // put the results of the query in dict2
                                                 NSDictionary *dict2 = [dict objectForKey:@"results"];
                                                 
                                                 // If there is just 1 stock, json is a dict
                                                 if(self.arrPortfolio.count < 2) {
                                                     self.dictJson3 = [dict2 objectForKey:@"quote"];
                                                 } else {
                                                 // multiple stocks, json is an array
                                                     self.arrJson   = [dict2 objectForKey:@"quote"];
                                                 }
                                                 
                                                 
                                                // self.dictJson3 = [self.arrJson objectAtIndex:0];
                                                 
                                                // self.arrJsonResult= [self.dictJson2 objectForKey:@"Result"];
                                                 
                                                 
                                                 
                                               //  NSLog(@"arrjson: %@", [self.arrJson objectAtIndex:0] );
                                                 /*
                                                  NSMutableDictionary *mDict = [dict2 objectForKey:@"quote"];
                                                  NSLog(@"MUTABLE DICTIONARY!");
                                                  NSLog(@"mDict Ticker: %@", [mDict objectForKey:@"Symbol"]);
                                                  NSLog(@"mDict Name: %@", [mDict objectForKey:@"Name"]);
                                                  */
                                                 
                                               //  NSLog(@"Ticker: %@", [dict2 objectForKey:@"Symbol"]);
                                       //          NSLog(@"Name: %@",   [self.dictJson3 objectForKey:@"Name"]);
                                                 
                                                 /*
                                                  NSLog(@"Ask: %@", [dict3 objectForKey:@"Ask"]);
                                                  NSLog(@"Change: %@", [dict3 objectForKey:@"PercentChange"]);
                                                  NSLog(@"YearRange: %@", [dict3 objectForKey:@"YearRange"]);
                                                  NSLog(@"PERatio: %@", [dict3 objectForKey:@"PERatio"]);
                                                  
                                                  NSLog(@"Yield: %@", [dict3 objectForKey:@"DividendYield"]);
                                                  NSLog(@"Dividend: %@", [dict3 objectForKey:@"DividendShare"]);
                                                  NSLog(@"Currency: %@", [dict3 objectForKey:@"Currency"]);
                                                  
                                                  NSLog(@"Payout Date: %@", [dict3 objectForKey:@"DividendPayDate"]);
                                                  NSLog(@"ExDividendDate: %@", [dict3 objectForKey:@"ExDividendDate"]);
                                                  */
                                                 NSLog(@"Reloading. Portfolio: %lu", (unsigned long)self.arrPortfolio.count);
                                                 NSLog(@"crash is because of reloading for some reason");
                                                 
                                                 
                                                 [self.activityIndicator stopAnimating];
                                                 [self loadUserDefaults];
                                                 [self.tableView reloadData];
                                                 
                                             });
                                         
                                         
                                         } else {
                                             
                                             NSLog(@"Couldnot parse json: %@", parsingError);
                                             
                                         }
                                         
                                     }];
    
    [task resume];
    [self.view endEditing:YES];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    
    NSLog(@"portfolio count %lu", (unsigned long)self.arrPortfolio.count);
   // NSLog(@"arrportfolio: %@", self.arrPortfolio);
    
    return self.arrPortfolio.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSLog(@"Tableview called");
    
    PortfolioCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell2" forIndexPath:indexPath];
    
    // pointer to each row, dont care about implicit conversion, xcode is dumb
    int index = indexPath.row;
    NSLog(@"count: %lu index: %d arrjson: %lu", (unsigned long)self.arrPortfolio.count, index,(unsigned long)self.arrJson.count);
    
    // this is so table view data is skipped before a new json is gotten.
    if(index <  self.arrJson.count )  {
        
    if(self.arrPortfolio.count > 1) {
        NSLog(@"count check called: arrJson count: %lu", (unsigned long)self.arrJson.count);
        
        // pointer to each stock
        self.dictJson3 = self.arrJson[index];
    }
   
    cell.lblTick.alpha  = 0;
    cell.lblName.alpha  = 0;
    cell.lblDate.alpha  = 0;
    cell.lblYield.alpha = 0;
    
    
    if([[self.dictJson3 objectForKey:@"Name"] isEqual:[NSNull null]]) {
        NSLog(@"Stock is null");
        cell.lblName.text = @"Error: Stock has no name";
    } else {
    cell.lblTick.text   = [self.dictJson3 objectForKey:@"Symbol"];
    cell.lblName.text   = [self.dictJson3 objectForKey:@"Name"];

    
    cell.lblDate.text   = [NSString stringWithFormat: @"Payout: %@",
                          [self.dictJson3 objectForKey:@"DividendPayDate"]];
    
    cell.lblYield.text  = [NSString stringWithFormat: @"Yield: %@%@",
                          [self.dictJson3 objectForKey:@"DividendYield"], @"%"];
    
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.9];
    cell.lblTick.alpha  = 1;
    cell.lblName.alpha  = 1;
    cell.lblDate.alpha  = 1;
    cell.lblYield.alpha = 1;
  //  cell.lblTick.transform = CGAffineTransformMakeScale(1, 1);
    [UIView commitAnimations];
 //   cell.lblTick.transform = CGAffineTransformIdentity;
    
    
    NSLog(@"symbol?: %@", [self.dictJson3 objectForKey:@"Symbol"]);

    //   cell.textLabel.text = [dictJsonResult objectForKey:@"symbol"];
    
    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    //   cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    
    if (indexPath.row%2 == 0) {
        UIColor *altCellColor = [UIColor colorWithWhite:0.2 alpha:0.1];
        cell.backgroundColor = altCellColor;
        
    }
    
    }
    return cell;
        
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIAlertView *messageAlert = [[UIAlertView alloc]
                                 initWithTitle:@"Row Selected" message:@"You've selected a row" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //  [messageAlert show];
    
    PortfolioCell *cell = (PortfolioCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    NSLog(@"Cell Pressed: %@", cell.lblTick.text);
    
    //Lets get the stock info and then prepare for seque
 // PUT THIS BACK!   [self createStockObj:cell.lblTick.text];
    
}
/*
-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *CellIdentifier = @"myCell2";
    UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
    
    // Yes color matching is awful
    self.navigationController.navigationBar.titleTextAttributes =
    [NSDictionary dictionaryWithObject:[UIColor blueColor] forKey:NSForegroundColorAttributeName];
    
    
    if (headerView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    return headerView;
}
*/
/*
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45.0f;
}
 */

#pragma mark - Navigation
/*
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    NSLog(@"prepareForSegue Called");
    
    if([segue.identifier isEqualToString:@"stockToPortfolio"]) {
        
     //   StockCtrl           *nextCtrl = [segue destinationViewController];
        PortfolioCtrl    *thisCtrl = sender; // whats this good for? self?
        
      //  nextCtrl.dictJson4 =  thisCtrl.dictJson3;
        
        NSLog(@"Prepared");
        
        
        //       nextCtrl.dict4 = self.dict3; // pass the stock data to StockCtrl
        
    }
    
    else if([segue.identifier isEqualToString:@"favorites"]) {
        NSLog(@"Preparing for Seque: %@", @"Alternative seque");
    }
    
    
}
 */
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
