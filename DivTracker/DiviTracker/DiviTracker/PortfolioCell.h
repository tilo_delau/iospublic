//
//  PortfolioCell.h
//  DiviTracker
//
//  Created by IT-Högskolan on 2015-07-17.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PortfolioCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTick;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblYield;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@end
