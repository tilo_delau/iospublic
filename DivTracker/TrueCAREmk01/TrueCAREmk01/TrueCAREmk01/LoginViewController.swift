//
//  LoginViewController.swift
//  TrueCAREmk01
//
//  Created by IT-Högskolan on 2015-09-08.
//  Copyright (c) 2015 tilo. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
/*
class prepareForSegue {
    <#properties and methods#>
} (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:@"searchToDetail"]) {
        
        SearchDetailCtrl    *nextCtrl = [segue destinationViewController];
        SearchCtrl          *thisCtrl = sender; // whats this good for? self?
        
        nextCtrl.dictJson2 =  thisCtrl.dictJson;
        
        //       nextCtrl.dict4 = self.dict3; // pass the stock data to StockCtrl
        
    }
    
    else if([segue.identifier isEqualToString:@"searchToPortfolio"]) {
        NSLog(@"Preparing for Seque: %@", @"Alternative seque");
}
*/
