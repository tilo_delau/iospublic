//
//  ViewController.m
//  hello_yahoo
//
//  Created by IT-Högskolan on 2015-05-22.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    
    NSString *searchString = @"http://query.yahooapis.com/v1/public/yql?q=select*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22STAR.ST%22)%0A%09%09&env=http%3A%2F%2Fdatatables.org%2Falltables.env&format=json";
    
    NSURL *url =[NSURL URLWithString:searchString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                
                                                NSError *parsingError = nil;
                                                
                                                NSDictionary *root = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:
                                                                      &parsingError];
                                                
                                                if (!parsingError) {
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        NSDictionary *dict = [root objectForKey:@"query"];
                                                        NSLog(@"Dict: %@", dict);
                                                        NSLog(@"valuejson: %@", [dict objectForKey:@"created"]);
                                       
                                                        NSDictionary *dict2 = [dict objectForKey:@"results"];
                                                        NSLog(@"Dict2: %@", dict2);
                                                        
                                                        NSDictionary *dict3 = [dict2 objectForKey:@"quote"];
                                                        NSLog(@"Dict3: %@", dict3);
                                                        
                                                        NSLog(@"Ticker: %@", [dict3 objectForKey:@"Symbol"]);
                                                        NSLog(@"Name: %@", [dict3 objectForKey:@"Name"]);
                                                        NSLog(@"Ask: %@", [dict3 objectForKey:@"Ask"]);
                                                        NSLog(@"Change: %@", [dict3 objectForKey:@"PercentChange"]);
                                                        NSLog(@"YearRange: %@", [dict3 objectForKey:@"YearRange"]);
                                                        NSLog(@"PERatio: %@", [dict3 objectForKey:@"PERatio"]);
                                                        
                                                        NSLog(@"Yield: %@", [dict3 objectForKey:@"DividendYield"]);
                                                        NSLog(@"Dividend: %@", [dict3 objectForKey:@"DividendShare"]);
                                                        NSLog(@"Currency: %@", [dict3 objectForKey:@"Currency"]);
                                                        
                                                        NSLog(@"Payout Date: %@", [dict3 objectForKey:@"DividendPayDate"]);
                                                        NSLog(@"ExDividendDate: %@", [dict3 objectForKey:@"ExDividendDate"]);
                                                        
                                                        
                                                        
                                                        
                                                    });
                                                    
                                                } else {
                                                    
                                                    NSLog(@"Couldnot parse json: %@", parsingError);
                                                    
                                                }
                                                
                                            }];
    
    [task resume];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
