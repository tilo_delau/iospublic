//
//  AppDelegate.h
//  PongZ
//
//  Created by IT-Högskolan on 2015-02-14.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

