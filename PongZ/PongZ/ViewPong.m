//
//  ViewPong.m
//  PongZ
//
//  Created by IT-Högskolan on 2015-02-14.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import "ViewPong.h"

@interface ViewPong ()

@property (weak, nonatomic) IBOutlet UIImageView *zBall;
@property (weak, nonatomic) IBOutlet UIButton *btnStart;
@property (weak, nonatomic) IBOutlet UILabel *winLose;
@property (weak, nonatomic) IBOutlet UIButton *btnQuit;

@property (weak, nonatomic) IBOutlet UILabel *ptsPlayer;
@property (weak, nonatomic) IBOutlet UILabel *ptsAI;
@property (weak, nonatomic) IBOutlet UIImageView *AI;
@property (weak, nonatomic) IBOutlet UIImageView *player;

@property BOOL hasTriggered;

@property (nonatomic) int X;
@property (nonatomic) int Y;

@property (nonatomic) NSURL *url;


@end


@implementation ViewPong


- (int) ScreenWidth {
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    
    return screenWidth;
}

- (int) ScreenHeight {
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenHeight = screenSize.height;
    
    return screenHeight;
}

- (void) Collision {
    
    if(CGRectIntersectsRect(self.zBall.frame, self.player.frame)) {
        
        self.Y = arc4random_uniform(5);
        self.Y = 0 - self.Y;
        
        self.X = (self.zBall.center.x - self.player.center.x) / 5;
        
        NSLog(@"X: %i", self.X);

        
        //AudioToolBox Sound (Not working)
        AudioServicesPlaySystemSound(PlaySoundID);
        
        // AVAudio working
        NSError *error;
        audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:self.url error:&error];
        
        [audioPlayer play];
    }
    
    if(CGRectIntersectsRect(_zBall.frame, _AI.frame)) {
        
        self.Y = arc4random_uniform(5);
        // AVAudio working
        NSError *error;
        audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:self.url error:&error];
        
        [audioPlayer play];
    }
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches]anyObject];
    CGPoint location = [touch locationInView:touch.view];
    
   // CGRect

//    CGRectContainsPoint(100, <#CGPoint point#>);
    
    
    if(CGRectContainsPoint(self.player.frame, location)) {
        
    self.player.center = [touch locationInView:self.view];
    
    if (self.player.center.y > [self ScreenHeight]-30) self.player.center = CGPointMake(self.player.center.x,[self ScreenHeight]-30);

    if (self.player.center.y < [self ScreenHeight]-30) self.player.center = CGPointMake(self.player.center.x,[self ScreenHeight]-30);

    if (self.player.center.x < 25)  self.player.center = CGPointMake(25, self.player.center.y);
    
    if (self.player.center.x > [self ScreenWidth]-25) self.player.center = CGPointMake([self ScreenWidth]-25, self.player.center.y);
    }
    
}

- (void) AIMovement {
    
    
    if (_AI.center.x > _zBall.center.x) {
        _AI.center = CGPointMake(_AI.center.x -(arc4random_uniform(2)), _AI.center.y);
    }
    if (_AI.center.x < _zBall.center.x) {
        _AI.center = CGPointMake(_AI.center.x +(arc4random_uniform(2)), _AI.center.y);
    }
    if (_AI.center.x < 15) {
        _AI.center = CGPointMake(15, _AI.center.y);
    }
    if (_AI.center.x > [self ScreenWidth]-25) {
        _AI.center = CGPointMake([self ScreenWidth]-25, _AI.center.y);
    }
    
    
}

- (IBAction)btnStart:(id)sender {
    
    AudioServicesPlaySystemSound(PlaySoundID);
    
    _btnStart.hidden    = YES;
    _zBall.hidden       = NO;
    
    
    self.Y = arc4random_uniform(11);
    self.Y = self.Y - 5;
    
    self.X = arc4random_uniform(11);
    self.X = self.X - 5;
    
    if (self.Y == 0) self.Y = 1;
    if (self.X == 0) self.X = 1;
    
    
    timer = [NSTimer scheduledTimerWithTimeInterval:speed target:self selector:@selector(BallMovement) userInfo:nil repeats:YES];
    
    _player.center = CGPointMake([self ScreenWidth]/2,[self ScreenHeight]-30);
    
}

- (void) BallMovement {
    
    [self AIMovement];
    [self Collision];
    _zBall.center = CGPointMake(_zBall.center.x+self.X, _zBall.center.y+self.Y);
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    
//    NSLog(@"Width: %f", screenWidth);
//    NSLog(@"Length: %f", screenHeight);
    
    
    if(_zBall.center.x < 15)  self.X = 0 - self.X;
    if(_zBall.center.x > screenWidth - 15) self.X = 0 - self.X;
    
    
    if(_zBall.center.y < 15)  self.Y = 0 - self.Y;
    if(_zBall.center.y > screenHeight - 15) self.Y = 0 - self.Y;
 
    if(_zBall.center.y < 0+15) {
        speed = speed - 0.01;
        PlayerPts++;
        _ptsPlayer.text = [NSString stringWithFormat:@"%i",PlayerPts];
        
        [timer invalidate];
        _zBall.hidden = YES;
        _AI.center = CGPointMake(135, 30);
//      _zBall.center = CGPointMake(145, 267);
        
        timer2 = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(btnStart:) userInfo:nil repeats:NO];
        
        if(PlayerPts == 3) {
            _btnStart.hidden = YES;
            _btnQuit.hidden  = NO;
            _winLose.hidden  = NO;
            _winLose.text    = [NSString stringWithFormat:@"You Win!"];
            _winLose.textColor = [UIColor blueColor];
            [timer2 invalidate];
            

        }
    }
    
    if(_zBall.center.y > screenHeight - 15) {
        
        AIpts++;
        _ptsAI.text = [NSString stringWithFormat:@"%i",AIpts];
    
        
        [timer invalidate];
 //       _btnStart.hidden = NO;
        _zBall.hidden = YES;
        _AI.center = CGPointMake(135, 30);
        _zBall.center = CGPointMake(145, 267);
        
        timer2 = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(btnStart:) userInfo:nil repeats:NO];
        
        if(AIpts == 3) {
            _btnStart.hidden = YES;
            _btnQuit.hidden  = NO;
            _winLose.hidden  = NO;
            _winLose.text    = [NSString stringWithFormat:@"You Lose!"];
            _winLose.textColor = [UIColor magentaColor];
            [timer2 invalidate];

        }
    }
  
}

/*
- (void)viewWillLayoutSubviews {
    
    NSLog(@"foobar");
    
}*/

- (void)viewDidLayoutSubviews {
    
    if(!self.hasTriggered) {

        self.hasTriggered = YES;
    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    _player.center = CGPointMake([self ScreenWidth]/2,[self ScreenHeight]-30);
    NSLog(@"foobar");
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    AIpts = 0;
    PlayerPts = 0;
    speed = 0.03;
/*
    // Not working
    NSURL *SoundURL =
    [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"spring_sound" ofType:@"mp3"]];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)SoundURL,&PlaySoundID);
    
    //AVFoundation Sound (loading url here so playing sound is faster later)
    self.url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/spring_sound.mp3", [[NSBundle mainBundle] resourcePath]]];
    [audioPlayer play]; // sound not working here.
*/
    NSLog(@"launch banner with clip");
    NSURL *clip = [[NSBundle mainBundle] URLForResource: @"spring_sound" withExtension:@"mp3"];
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:clip error:NULL];
    [audioPlayer play];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
